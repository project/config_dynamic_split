<?php

namespace Drupal\config_dynamic_split\Entity;

use Drupal\config_dynamic_split\ConfigFilterTrait;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\StorageCopyTrait;
use Drupal\Core\Config\StorageInterface;

/**
 * Defines the Firewall Policy entity.
 *
 * @ConfigEntityType(
 *   id = "config_dynamic_split",
 *   label = @Translation("Config Dynamic Split"),
 *   translatable = FALSE,
 *   config_prefix = "split",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   config_export = {
 *     "id",
 *     "description",
 *     "value",
 *     "storage",
 *     "configuration",
 *   }
 * )
 */
class ConfigDynamicSplit extends ConfigEntityBase {

  use ConfigFilterTrait;
  use StorageCopyTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type) {
    if (!array_key_exists('configuration', $values) || !is_array($values['configuration'])) {
      $values['configuration'] = [];
    }
    parent::__construct($values, $entity_type);
    $this->tokenizer = \Drupal::service('token');
  }

  /**
   * Get the parsed value for this split.
   *
   * @return string
   *   The 'value' for this dynamic split, such as 'example.com' for domain.
   */
  public function getValue() {
    $cds = $this->createDuplicate();
    $cds->id = $this->id;
    $result = $this->tokenizer->replace($this->value, ['config_dynamic_split' => $cds]);

    // If there are any unreplaced tokens, return an empty string.
    return (strpos($result, '[') === FALSE) ? $result : '';
  }

  /**
   * Get the storage location for this split.
   *
   * @return string
   *   The filesystem directory where this config-split is stored.
   */
  public function getStorageLocation() {
    $cds = $this->createDuplicate();
    $cds->id = $this->id;
    return $this->tokenizer->replace($this->storage, ['config_dynamic_split' => $cds]);
  }

  /**
   * Get a filtered config storage object for the split config.
   *
   * This provides only the config identified in this split configuration.
   *
   * @param \Drupal\Core\Config\StorageInterface $sourceStorage
   *   (optional) Config storage. Defaults to the config available to this
   *   split.
   *
   * @return \Drupal\Core\Config\StorageInterface
   *   An ephemeral memory-storage filtered to the config that applies to this
   *   split.
   */
  public function getStorage(StorageInterface $sourceStorage = NULL) {
    if (is_null($sourceStorage)) {
      $sourceStorage = $this->getRawStorage();
    }
    return self::filterStorage($sourceStorage, $this->getSplitDefinition());
  }

  /**
   * Persistently store a split storage.
   *
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   The storage with the values to persistent store.
   */
  public function commit(StorageInterface $storage) {
    $target = $this->getRawStorage();
    $this->replaceStorageContents($target, $storage);
  }

  /**
   * Get the list of configuration names and properties to be split.
   *
   * For example:
   * [
   *   'system.date' => [
   *     'timezone.default',
   *   ]
   * ];
   *
   * @return array
   *   Each element is indexed by the configuration name, and provides an array
   *   of properties to split against.
   */
  public function getSplitDefinition() {
    // Replace the ':' identifier in the configuration names with a '.'.
    $newKeys = array_map(function ($value) {
      return str_replace(':', '.', $value);
    }, array_keys($this->configuration));

    return array_combine($newKeys, $this->configuration);
  }

  /**
   * Report whether this split is active.
   *
   * @return bool
   *   TRUE if this dynamic split is in use.
   */
  public function isActive() {
    return $this->getValue() && count($this->getStorage()->listAll());
  }

  /**
   * Get a config storage object for the split config.
   *
   * @return \Drupal\Core\Config\StorageInterface
   *   Config storage interface providing the split config from the filesystem.
   */
  protected function getRawStorage() {
    return new FileStorage($this->getStorageLocation());
  }

}

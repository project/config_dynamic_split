<?php

namespace Drupal\config_dynamic_split\EventSubscriber;

use Drupal\config_dynamic_split\ConfigFilterTrait;
use Drupal\config_dynamic_split\Entity\ConfigDynamicSplit;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigEvents as CoreConfigEvents;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\StorageTransformEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to configuration import and export events.
 */
class ConfigEvents implements EventSubscriberInterface {

  use ConfigFilterTrait;

  /**
   * Primary filesystem storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $fileStorage;

  /**
   * ConfigEvents constructor.
   *
   * @param \Drupal\Core\Config\StorageInterface $file_storage
   *   The sensitive config manager service.
   */
  public function __construct(StorageInterface $file_storage) {
    $this->fileStorage = $file_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[CoreConfigEvents::STORAGE_TRANSFORM_IMPORT][] = ['onImportTransform'];
    $events[CoreConfigEvents::STORAGE_TRANSFORM_EXPORT][] = ['onExportTransform'];
    return $events;
  }

  /**
   * The storage is transformed for importing.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The event for altering configuration of the storage.
   */
  public function onImportTransform(StorageTransformEvent $event) {
    $storage = $event->getStorage();

    // Apply the split-config as an override to the global config sync.
    foreach ($this->getSplitEntities() as $split) {
      $splitStorage = $split->getStorage();
      foreach ($splitStorage->listAll() as $key) {
        $sourceVal = $storage->read($key) ? $storage->read($key) : [];
        $splitVal = $splitStorage->read($key);

        $config = NestedArray::mergeDeep($sourceVal, $splitVal);
        $storage->write($key, $config);
      }
    }
  }

  /**
   * The storage is transformed for exporting.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The event for altering configuration of the storage.
   */
  public function onExportTransform(StorageTransformEvent $event) {
    $storage = $event->getStorage();

    foreach ($this->getSplitEntities() as $split) {
      // Create a copy of the DB storage, filtered to the items allowed in the
      // split.
      $splitStorage = $split->getStorage($storage);

      // Compare with the unsplit *filesystem* storage. Remove items that are
      // identical, so only the diff is recorded in the split.
      foreach ($splitStorage->listAll() as $key) {
        // Dedupe values only if the property exists in the unsplit config.
        // This ensures split-only config is not removed.
        if ($this->fileStorage->exists($key)) {
          $config = self::nestedArrayDedupeDeep($splitStorage->read($key), $this->fileStorage->read($key));
          if (empty($config)) {
            $splitStorage->delete($key);
          }
          else {
            $splitStorage->write($key, $config);
          }
        }
      }

      // Ensure the original data is retained in the unsplit storage.
      $filteredStorage = self::filterStorage($this->fileStorage, $split->getSplitDefinition());
      foreach ($filteredStorage->listAll() as $key) {
        $config = NestedArray::mergeDeep($storage->read($key), $filteredStorage->read($key));
        $storage->write($key, $config);
      }

      // Copy the ephemeral storage to the persistent split storage.
      $split->commit($splitStorage);
    }
  }

  /**
   * Get all the dynamic-splits that are available.
   *
   * @return \Drupal\config_dynamic_split\Entity\ConfigDynamicSplit[]
   *   An array of all ConfigDynamicSplit entities.
   */
  protected function getSplitEntities() {
    return ConfigDynamicSplit::LoadMultiple();
  }

  /**
   * Remove items from an array if they are identical.
   *
   * @param array $array1
   *   The array from which duplicates should be removed.
   * @param array $array2
   *   The source array for comparison.
   *
   * @return array
   *   The resulting deduped array.
   */
  protected static function nestedArrayDedupeDeep(array $array1, array $array2) {
    $result = $array1;
    foreach ($array1 as $key => $val) {
      if (is_array($val)) {
        $result[$key] = self::nestedArrayDedupeDeep($array1[$key], $array2[$key]);
      }
      elseif ($val === $array2[$key]) {
        unset($result[$key]);
      }
    }

    // Remove empty array-keys.
    return array_filter($result, function ($value) {
      return !(is_array($value) && count($value) === 0);
    });
  }

}

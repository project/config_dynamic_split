<?php

namespace Drupal\config_dynamic_split\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Consolidation\OutputFormatters\Options\FormatterOptions;
use Drupal\config_dynamic_split\Entity\ConfigDynamicSplit;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Commands\DrushCommands;

/**
 * Drush commands for Config - Dynamic Split.
 */
class CdsCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * List the splits and mask whether they are active.
   *
   * @command cds:list
   *
   * @field-labels
   *   id: ID
   *   description: Description
   *   value: Value
   *   storage: Storage directory
   *   count: Count
   *   active: Is active?
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Structured data to present as a table.
   */
  public function list() {
    $output = [];
    foreach ($this->getSplits() as $split) {
      $split->getStorage();

      $output[] = [
        'id' => $split->id(),
        'description' => $split->description,
        'value' => $split->getValue(),
        'storage' => $split->getStorageLocation(),
        'count' => count($split->getStorage()->listAll()),
        'active' => $split->isActive(),
      ];
    }
    $data = new RowsOfFields($output);

    $data->addRendererFunction(
      function ($key, $cellData, FormatterOptions $options, $rowData) {
        switch ($key) {
          case 'active':
            return ($cellData) ? $this->t('Yes') : $this->t('No');

          case 'storage':
          case 'count':
            return ($rowData['value']) ? $cellData : '';

          default:
            return $cellData;
        }
      });

    return $data;
  }

  /**
   * Load all the dynamic splits.
   *
   * @return \Drupal\config_dynamic_split\Entity\ConfigDynamicSplit[]
   *   An array of all ConfigDynamicSplit entities.
   */
  protected function getSplits() {
    return ConfigDynamicSplit::loadMultiple();
  }

}

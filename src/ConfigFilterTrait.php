<?php

namespace Drupal\config_dynamic_split;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\MemoryStorage;
use Drupal\Core\Config\StorageCopyTrait;
use Drupal\Core\Config\StorageInterface;

/**
 * Trait for manipulating config storage.
 */
trait ConfigFilterTrait {

  use StorageCopyTrait;

  /**
   * Filter a storage collection against a list of allowed keys/properties.
   *
   * @param \Drupal\Core\Config\StorageInterface $source
   *   The configuration storage to filter.
   * @param array $filterDefinition
   *   The keys/properties that are permitted.
   *
   * @return \Drupal\Core\Config\StorageInterface
   *   An ephemeral memory storage, with the allowed keys and properties.
   */
  protected static function filterStorage(StorageInterface $source, array $filterDefinition) {
    // Copy to ephemeral storage so that the original won't be changed.
    $ephemeralStorage = new MemoryStorage();
    self::replaceStorageContents($source, $ephemeralStorage);

    // Pass 1: filter out entire config objects.
    $invalidKeys = array_diff($ephemeralStorage->listAll(), self::getValidKeys($ephemeralStorage, $filterDefinition));
    foreach ($invalidKeys as $key) {
      $ephemeralStorage->delete($key);
    }

    // Pass 2: where a config has property-specific constraints, remove the
    // un-split properties.
    $constrainedConfig = array_filter($filterDefinition);
    foreach ($constrainedConfig as $key => $properties) {
      if ($config = $ephemeralStorage->read($key)) {
        $approvedConfig = [];
        foreach ($properties as $property) {
          $parts = explode('.', $property);
          if (NestedArray::keyExists($config, $parts)) {
            NestedArray::setValue($approvedConfig, $parts, NestedArray::getValue($config, $parts));
          }
        }
        $ephemeralStorage->write($key, $approvedConfig);
      }
    }

    return $ephemeralStorage;
  }

  /**
   * Get a list of the config keys deemed as valid.
   *
   * Note that wildcards in the config definition are supported, such as
   * 'block.*'.
   *
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   Config storage.
   * @param array $filterDefinition
   *   The keys/properties that are permitted.
   *
   * @return array
   *   An array giving each of the config *keys* that apply to the specified
   *   storage.
   */
  protected static function getValidKeys(StorageInterface $storage, array $filterDefinition) {
    // Keys with a 100% match are valid.
    $validKeys = array_values(array_intersect($storage->listAll(), array_keys($filterDefinition)));

    // Iterate the wildcard keys.
    $wildcards = array_filter(array_keys($filterDefinition), function ($val) {
      return strpos($val, '*') !== FALSE;
    });

    foreach ($wildcards as $key) {
      $regex = '/' . str_replace('\*', '.*', preg_quote($key)) . '/';
      foreach ($storage->listAll() as $skey) {
        if (!in_array($skey, $validKeys) && preg_match($regex, $skey)) {
          $validKeys[] = $skey;
        }
      }
    }

    return $validKeys;
  }

}

# Config Dynamic Split module

## Purpose

The _Config – Dynamic Split_ module allows config to be automatically
segmented by token-based criteria, such as *environment*, *domain name*,
*cluster*, or any parameter which is available as a global token.

## Behaviours

1. Splits can be managed at a config _property_ level.
   For example, `system.date`:`timezone.default`, rather than an entire config
   object such as `system.date`.
2. A split-file will only be generated if the value differs from that specified
   in the global sync.

## Default storage

By default, the module will stored dynamic-split configuration in a directory
which is relative to the defined config-sync directory:

- ../config-dynamic-split

### Recommended storage directory structure

It's recommended to use the ID of the dynamic config-split as the first level
directory, and the value of the token as the second level.
For example, if the config is segmented by environment, the directories may be:

- ../config-dynamic-split/environment/dev
- ../config-dynamic-split/environment/stage
- ../config-dynamic-split/environment/prod

Where `dev`, `stage`, and `prod` represent token-values for the `environment`
global token.
